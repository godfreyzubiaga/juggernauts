import './App.css';

import { Layout } from 'antd';
import React, { useEffect, useState } from 'react';
import { BrowserView, MobileView } from 'react-device-detect';
import ReactGA from 'react-ga';
import styled from 'styled-components';

import { logoIcon } from './assets/images';
import LoadingOverlay from './components/LoadingOverlay';
import SideBarMenu from './components/SideBar/SideBar';
import Routes from './Routes';

const { Content } = Layout;

const StyledDiv = styled.div`
  min-height: calc(100vh - 80px);
  display: flex;
  justify-content: center;
  align-items: center;
  flex-direction: column;
`;

const Container = styled.div`
  height: 500px;
  display: flex;
  justify-content: space-evenly;
  align-items: center;
  flex-direction: column;
`;

const Title = styled.p`
  margin: 0;
  font-size: 1.3em;
  font-weight: bold;
`;

const Subtitle = styled.p`
  margin: 0;
  font-size: 1em;
`;

const LogoIcon = styled.div`
  background: url(${logoIcon}) no-repeat;
  background-size: contain;
  background-position: center center;
  height: 200px;
  width: 200px;
  /* border: 1px solid black; */
`;

const App = (): JSX.Element => {
  const [isLoading, setIsLoading] = useState(true);

  useEffect(() => {
    ReactGA.initialize('UA-186918008-1');

    setIsLoading(false);
  }, []);

  return (
    <>
      <BrowserView>
        <Layout>
          <SideBarMenu />
          <Layout style={{ height: '100vh' }}>
            {isLoading && <LoadingOverlay />}
            <Content>
              <Routes />
            </Content>
          </Layout>
        </Layout>
      </BrowserView>
      <MobileView>
        <StyledDiv>
          <Container>
            <LogoIcon />
            <Subtitle>Site is not yet available for mobile view.</Subtitle>
            <Title>Sorry!</Title>
          </Container>
        </StyledDiv>
      </MobileView>
    </>
  );
};

export default App;
