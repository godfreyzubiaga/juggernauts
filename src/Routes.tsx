import React from 'react';
import { Switch, Route } from 'react-router';
import AccountsPage from './pages/Accounts/AccountsPage';
import EnvironmentsPage from './pages/Environments/Environments';
import Homepage from './pages/Juggernauts/Juggernauts';
import JenkinsPage from './pages/Jenkins/Jenkins';
import PageNotFound from './pages/PageNotFound';
import LinksPage from './pages/Links/LinksPage';

const Routes = (): JSX.Element => (
  <Switch>
    <Route component={Homepage} path={['/juggernauts', '/']} exact />
    <Route component={AccountsPage} path={['/accounts/:env']} exact />
    <Route component={EnvironmentsPage} path={['/environments']} exact />
    <Route component={JenkinsPage} path={['/jenkins/:key']} exact />
    <Route component={LinksPage} path={['/links']} exact />
    <Route component={PageNotFound} />
  </Switch>
);

export default Routes;
