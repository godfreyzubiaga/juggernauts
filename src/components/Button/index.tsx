import React from 'react';
import styled from 'styled-components';

import {
  activeDangerColor,
  activePrimaryColor,
  activeSecondaryColor,
  activeWhite,
  black,
  dangerColor,
  primaryColor,
  secondaryColor,
  white,
} from '../../utils/colors';

const primaryButtonStyle = `
  background: ${primaryColor};
  color: ${white};

  &:hover {
    transition-duration: 0.3s;
    background: ${activePrimaryColor};
  }
`;

const clearButtonStyle = `
  background: ${white};
  color: ${black};

  &:hover {
    transition-duration: 0.3s;
    background: ${activeWhite};
  }
`;

const secondaryButtonStyle = `
  background: ${secondaryColor};
  color: ${white};

  &:hover {
    transition-duration: 0.3s;
    background: ${activeSecondaryColor};
  }
`;

const dangerButtonStyle = `
  background: ${dangerColor};
  color: ${white};

  &:hover {
    transition-duration: 0.3s;
    background: ${activeDangerColor};
  }
`;

interface ButtonStyleProps {
  buttonStyle?: string;
  customStyle?: string;
}

const StyledButton = styled.button<ButtonStyleProps>`
  border: none;
  border-radius: 5px;
  font-size: 0.85em;
  padding: 10px 15px;
  transition-duration: 0.3s;
  font-family: 'Muli', sans-serif;
  font-weight: bold;
  margin: 5px 0;
  color: white;
  cursor: pointer;

  &:hover {
    transition-duration: 0.3s;
    /* transform: translateY(-3px); */
    /* box-shadow: 0px 5px 10px rgba(0, 0, 0, 0.5); */
  }

  &:active,
  &:focus {
    outline: 0;
  }

  ${(props: ButtonStyleProps) => props.buttonStyle}
  ${(props: ButtonStyleProps) => props.customStyle}
`;

interface ButtonProps {
  name: string;
  onClick: (event: React.MouseEvent<HTMLButtonElement, MouseEvent>) => void;
  primary?: boolean;
  secondary?: boolean;
  danger?: boolean;
  clear?: boolean;
  customStyle?: string;
  type: 'button' | 'submit' | 'reset' | undefined;
}

const Button = ({ name, onClick, type, primary, secondary, danger, clear, customStyle }: ButtonProps): JSX.Element => {
  const getButtonStyle = () => {
    if (primary) return primaryButtonStyle;
    if (secondary) return secondaryButtonStyle;
    if (danger) return dangerButtonStyle;
    if (clear) return clearButtonStyle;
  };

  return (
    <StyledButton type={type} onClick={onClick} buttonStyle={getButtonStyle()} customStyle={customStyle}>
      {name}
    </StyledButton>
  );
};

export default Button;
