import moment from 'moment';
import React, { useEffect, useState } from 'react';
import styled from 'styled-components';

const TimeDiv = styled.div``;

const momentTimeFormat = 'dddd, MMMM Do YYYY, h:mm:ss a';

interface Props {
  customFormat?: string;
  utcOffset?: number;
}

const DigitalDateClock = ({ customFormat, utcOffset }: Props): JSX.Element => {
  const [time, setTime] = useState(
    moment(new Date().getTime())
      .utcOffset(utcOffset || 480)
      .format(customFormat || momentTimeFormat),
  );

  useEffect(() => {
    const timer = setInterval(getTime, 1000);

    return () => clearInterval(timer);
  }, []);

  const getTime = (): void => {
    const dateTime = moment(new Date().getTime())
      .utcOffset(utcOffset || 480)
      .format(customFormat || momentTimeFormat);
    setTime(dateTime);
  };

  return <TimeDiv>{time}</TimeDiv>;
};

export default DigitalDateClock;
