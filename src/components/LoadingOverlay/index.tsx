import React from 'react';
import { SyncLoader } from 'react-spinners';
import styled from 'styled-components';

const StyledDiv = styled.div`
  position: relative;
  height: inherit;
  width: inherit;
  border-radius: inherit;
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  z-index: 5;
  background: rgba(0, 0, 0, 0.05);
  color: white;
  cursor: progress;
`;

const LoadingText = styled.p`
  margin-top: 10px;
  color: white;
  font-size: 2em;
  background: linear-gradient(90deg, rgba(2, 0, 36, 1) 0%, rgba(251, 178, 23, 1) 0%, rgba(231, 45, 141, 1) 100%);
  -webkit-background-clip: text;
  background-clip: text;
  -webkit-text-fill-color: transparent;
  font-weight: bold;
`;

const LoadingOverlay = (): JSX.Element => (
  <StyledDiv>
    <SyncLoader size={15} color="rgba(231, 45, 141, 1)" loading />
    <LoadingText>Loading...</LoadingText>
  </StyledDiv>
);

export default LoadingOverlay;
