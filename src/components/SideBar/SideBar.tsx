import { Layout, Menu } from 'antd';
import SubMenu from 'antd/lib/menu/SubMenu';
import React, { useEffect, useState } from 'react';
import { TiGroup } from 'react-icons/ti';
import { IoLinkSharp } from 'react-icons/io5';
import { VscServerEnvironment } from 'react-icons/vsc';
import { useHistory } from 'react-router';
import styled from 'styled-components';

import { CiCircleOutlined, UserOutlined } from '@ant-design/icons';

import { logo } from '../../assets/images';
import { useTypedSelector } from '../../redux/reducers/rootReducer';
import DigitalDateClock from '../DigitalDateClock/DigitalDateClock';

const { Sider } = Layout;

const LogoContainer = styled.div`
  height: 100px;
  width: 100px;
  margin: 20px auto 20px auto;
  display: flex;
  justify-content: center;
  align-items: center;
`;

const Logo = styled.img`
  background-size: contain;
  background-position: center center;
  height: 100%;
`;

const TimeContainer = styled.div`
  height: 200px;
  display: flex;
  justify-content: space-evenly;
  flex-direction: column;
  font-size: 0.9em;
  color: white;
  text-align: center;
  position: absolute;
  box-sizing: border-box;
  width: 100%;
  bottom: 20px;
  left: 0;
`;

const SideBarMenu = (): JSX.Element => {
  const [selectedMenu, setSelectedMenu] = useState('');
  const [openKeys, setOpenKeys] = React.useState(['']);
  const paths = useTypedSelector((state) => state.paths);
  const history = useHistory();

  const rootSubmenuKeys = ['accounts', 'jenkins'];

  const onOpenChange = (keys: string[]) => {
    const latestOpenKey = keys.find((key) => openKeys.indexOf(key) === -1);
    if (latestOpenKey && rootSubmenuKeys.indexOf(latestOpenKey) === -1) {
      setOpenKeys(keys);
    } else {
      setOpenKeys(latestOpenKey ? [latestOpenKey] : []);
    }
  };

  useEffect(() => {
    const [, pathName, key] = location.pathname.split('/');
    if (key) {
      setSelectedMenu(pathName + '/' + key);
    } else {
      if (pathName === 'accounts' || pathName === 'jenkins') {
        const fallbackKey = paths[pathName][0];
        setSelectedMenu(key);
        history.push(`/${pathName}/${fallbackKey.key}`);
      }
    }
  }, []);

  const onMenuSelect = (selectedKey: string | React.ReactText[] | undefined) => {
    selectedKey && setSelectedMenu(selectedKey[0] as string);
    history.push('/' + selectedKey);
  };

  return (
    <Sider style={{ height: '100vh' }}>
      <LogoContainer>
        <Logo src={logo} alt="Juggernauts R&D" />
      </LogoContainer>
      <Menu
        selectedKeys={[selectedMenu]}
        onOpenChange={(e) => onOpenChange(e as string[])}
        openKeys={openKeys}
        onSelect={(a) => onMenuSelect(a.selectedKeys)}
        mode="inline"
        theme="dark"
      >
        <SubMenu key="accounts" icon={<UserOutlined />} title="SC Accounts">
          <Menu.ItemGroup key="env" title="Environments">
            <Menu.Item key="accounts/pt130">PT130</Menu.Item>
            <Menu.Item key="accounts/pt03">PT03</Menu.Item>
            <Menu.Item key="accounts/env4">ENV4</Menu.Item>
            <Menu.Item key="accounts/env9">ENV9</Menu.Item>
          </Menu.ItemGroup>
        </SubMenu>
        <SubMenu key="jenkins" icon={<CiCircleOutlined />} title="Jenkins Builds">
          <SubMenu key="core" title="Core Builds">
            <Menu.Item key="jenkins/core-csr" title="CSR">
              CSR
            </Menu.Item>
            <Menu.Item key="jenkins/core-sc" title="Self Care">
              Self Care
            </Menu.Item>
          </SubMenu>
          <SubMenu key="pldt" title="PLDT Builds">
            <Menu.Item key="jenkins/pldt-csr" title="CSR">
              CSR
            </Menu.Item>
            <Menu.Item key="jenkins/pldt-sc" title="Self Care">
              Self Care
            </Menu.Item>
          </SubMenu>
        </SubMenu>
        <Menu.Item key="links" title="links" icon={<IoLinkSharp style={{ color: 'white' }} />}>
          Useful Links
        </Menu.Item>
        <Menu.Item key="environments" title="environments" icon={<VscServerEnvironment style={{ color: 'white' }} />}>
          Environments
        </Menu.Item>
        <Menu.Item key="juggernauts" title="Juggernauts R&D" icon={<TiGroup style={{ color: 'white' }} />}>
          Juggernauts R&D
        </Menu.Item>
      </Menu>
      <TimeContainer>
        <div>
          <b>Manila</b>
          <DigitalDateClock customFormat="MMMM Do YYYY, h:mm:ss a" />
        </div>
        <div>
          <b>India</b>
          <DigitalDateClock utcOffset={330} customFormat="MMMM Do YYYY, h:mm:ss a" />
        </div>
        <div>
          <b>Israel/Cyprus</b>
          <DigitalDateClock utcOffset={120} customFormat="MMMM Do YYYY, h:mm:ss a" />
        </div>
      </TimeContainer>
    </Sider>
  );
};

export default SideBarMenu;
