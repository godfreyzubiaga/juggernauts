import './index.css';

import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { BrowserRouter } from 'react-router-dom';

import App from './App';
import store from './redux/store';
import reportWebVitals from './reportWebVitals';
import firebase from 'firebase/app';
import 'firebase/database';

const firebaseConfig = {
  apiKey: 'AIzaSyBcb3AOQyu4B-k09166IyPSauw_v9eLgu8',
  authDomain: 'juggernauts-rnd.firebaseapp.com',
  projectId: 'juggernauts-rnd',
  storageBucket: 'juggernauts-rnd.appspot.com',
  messagingSenderId: '931205888517',
  appId: '1:931205888517:web:cf437beb99376d2894ba6c',
  measurementId: 'G-4ZVFTWZ1JS',
};

const fb = firebase.initializeApp(firebaseConfig);
export const fbDatabase = fb.database();

ReactDOM.render(
  <Provider store={store}>
    <BrowserRouter>
      <App />
    </BrowserRouter>
  </Provider>,
  document.getElementById('root'),
);

reportWebVitals();
