import { notification, Tooltip } from 'antd';
import copy from 'copy-to-clipboard';
import React from 'react';
import { AiOutlineCopy } from 'react-icons/ai';
import styled from 'styled-components';

import { Account } from '../../redux/reducers/accountReducer';

const OutsideDiv = styled.div`
  display: inline-block;
  padding: 20px;
`;

const AccountDiv = styled.div`
  display: flex;
  justify-content: space-evenly;
  flex-direction: column;
  border: 1px solid lightgray;
  border-radius: 5px;
  width: 420px;
  height: 100px;
  padding: 0 20px;
  font-size: 1.2em;
  margin: 5px;
  background: rgba(255, 255, 255, 0.6);
`;

const ValueContainer = styled.div`
  display: flex;
  justify-content: space-between;
  flex-direction: row;
  font-size: 0.9em;
  color: #929292;
`;

const Value = styled.span`
  font-weight: bold;
  background: linear-gradient(90deg, rgba(2, 0, 36, 1) 0%, rgba(251, 178, 23, 1) 0%, rgba(231, 45, 141, 1) 100%);
  -webkit-background-clip: text;
  background-clip: text;
  -webkit-text-fill-color: transparent;
`;

const AccountContainer = ({ username, password, details }: Account): JSX.Element => {
  const copyToClipboard = (text: string) => {
    copy(text);
    notification.success({
      message: text,
      description: 'Succesfully added to your clipboard!',
      placement: 'bottomRight',
    });
  };

  return (
    <OutsideDiv>
      <AccountDiv>
        <ValueContainer>
          <div>
            Username: <Value>{username}</Value>
          </div>
          <Tooltip placement="rightTop" title="Copy Username">
            <AiOutlineCopy
              color="rgba(231, 45, 141, 1)"
              onClick={() => copyToClipboard(username)}
              cursor="pointer"
              size={22}
            />
          </Tooltip>
        </ValueContainer>
        <ValueContainer>
          <div>
            Password: <Value>{password}</Value>
          </div>
          <Tooltip placement="rightTop" title="Copy Password">
            <AiOutlineCopy
              color="rgba(231, 45, 141, 1)"
              onClick={() => copyToClipboard(password)}
              cursor="pointer"
              size={22}
            />
          </Tooltip>
        </ValueContainer>
        {details?.brand && (
          <ValueContainer>
            <div>
              Brand: <Value>{details.brand}</Value>
            </div>
          </ValueContainer>
        )}
      </AccountDiv>
    </OutsideDiv>
  );
};

export default AccountContainer;
