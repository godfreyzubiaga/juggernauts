import { Button, Dropdown, Empty, Menu } from 'antd';
import { MenuInfo } from 'rc-menu/lib/interface';
import React, { useEffect, useState } from 'react';
import { FaSimCard } from 'react-icons/fa';
import { useDispatch, useSelector } from 'react-redux';
import { useHistory } from 'react-router';
import styled from 'styled-components';
import { v4 as uuidv4 } from 'uuid';

import { DownOutlined } from '@ant-design/icons';

import { transparentLogo } from '../../assets/images';
import LoadingOverlay from '../../components/LoadingOverlay';
import { addAccount, recognizedENV, retrieveAccounts } from '../../redux/actions/accountActions';
import { Account } from '../../redux/reducers/accountReducer';
import { AppState } from '../../redux/types';
import { centerAllFlex } from '../../utils/styles';
import AccountContainer from './AccountContainer';
import AddAccountModal from './AddAccountModal';

interface StyleProps {
  noData: boolean;
}

const Container = styled.div`
  margin: 0 auto;
  padding: 100px;
  height: 100vh;
`;

const InnerContainer = styled.div`
  height: 100%;
  padding: 20px;
  border-radius: 20px;
  width: 100%;
  margin: 0 auto;
  text-align: center;
  background: url(${transparentLogo}) no-repeat;
  background-size: 20%;
  background-position: center center;
  background-color: #fafafa;
  display: flex;
  justify-content: space-between;
  align-items: center;
  flex-direction: column;
`;

const AccountDivs = styled.div<StyleProps>`
  display: block;
  height: 100%;
  width: 100%;
  margin: 0 auto;
  text-align: center;
  ${(props) => props.noData && centerAllFlex}
`;

const NoDataContainer = styled.span`
  display: flex;
  justify-content: center;
  flex-direction: column;
  height: 150px;
  padding: 10px;
  border-radius: 10px;
  background: rgba(255, 255, 255, 0.6);
`;

const StyledButton = styled(Button)`
  background: linear-gradient(90deg, rgba(2, 0, 36, 1) 0%, rgba(251, 178, 23, 1) 0%, rgba(231, 45, 141, 1) 100%);
  border: none;
  transition-duration: 0.3s;

  &:hover {
    background: linear-gradient(270deg, rgba(2, 0, 36, 1) 0%, rgba(251, 178, 23, 1) 0%, rgba(231, 45, 141, 1) 100%);
    transition-duration: 0.3s;
  }

  &:focus {
    background: linear-gradient(270deg, rgba(2, 0, 36, 1) 0%, rgba(251, 178, 23, 1) 0%, rgba(231, 45, 141, 1) 100%);
  }
`;

const AccountsPage = (): JSX.Element => {
  const accountState = useSelector((state: AppState) => state.account);
  const [modalVisible, setModalVisible] = useState(false);
  const [brand, setBrand] = useState('All');
  const brandList = ['All', 'Smart Postpaid', 'Smart Prepaid', 'Sun', 'TNT'];
  const [env, setEnv] = useState('');
  const history = useHistory();
  const dispatch = useDispatch();

  useEffect(() => {
    const env = location.pathname.split('/')[2] as recognizedENV;
    setEnv(env);
    dispatch(retrieveAccounts(env));
  }, [history.location.pathname]);

  const handleOk = (accountData: Account) => {
    setModalVisible(false);
    dispatch(addAccount(env as recognizedENV, accountData));
  };

  const handleMenuClick = (e: MenuInfo) => {
    setBrand(brandList[+e.key]);
    const env = location.pathname.split('/')[2] as recognizedENV;
    setEnv(env);
    dispatch(retrieveAccounts(env, brandList[+e.key]));
  };

  const BrandMenu = (
    <Menu onClick={(e) => handleMenuClick(e)}>
      {brandList.map((brandName: string, key: number) => (
        <Menu.Item key={key} icon={<FaSimCard color="rgba(251, 178, 23, 1)" style={{ marginRight: 5 }} />}>
          {brandName}
        </Menu.Item>
      ))}
    </Menu>
  );

  return (
    <Container>
      {modalVisible && (
        <AddAccountModal
          handleCancel={() => setModalVisible(false)}
          env={env}
          visible={modalVisible}
          handleOk={handleOk}
          hideModal={() => setModalVisible(false)}
        />
      )}
      <InnerContainer>
        <Dropdown overlay={BrandMenu}>
          <Button style={{ width: 160 }}>
            <FaSimCard style={{ marginRight: 5 }} color="rgba(251, 178, 23, 1)" /> {brand} <DownOutlined />
          </Button>
        </Dropdown>
        <AccountDivs noData={!accountState.status.loading && !!(accountState?.accounts?.length === 0)}>
          {accountState.status.loading && <LoadingOverlay />}
          {accountState?.accounts &&
            accountState.accounts.length > 0 &&
            accountState.accounts.map((account: Account) => <AccountContainer key={uuidv4()} {...account} />)}
          {!accountState.status.loading && accountState.accounts && accountState.accounts.length === 0 && (
            <NoDataContainer>
              <Empty description="No Data" />
            </NoDataContainer>
          )}
        </AccountDivs>
        <StyledButton loading={accountState.status.loading} type="primary" onClick={() => setModalVisible(true)}>
          Add new account
        </StyledButton>
      </InnerContainer>
    </Container>
  );
};

export default AccountsPage;
