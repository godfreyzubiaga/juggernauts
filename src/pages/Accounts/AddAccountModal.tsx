import { Button, Dropdown, Form, Input, Menu, Modal, Switch, Typography } from 'antd';
import { MenuInfo } from 'rc-menu/lib/interface';
import React, { useState } from 'react';
import { FaSimCard } from 'react-icons/fa';
import styled from 'styled-components';

import { DownOutlined } from '@ant-design/icons';

import { Account } from '../../redux/reducers/accountReducer';
import { centerAllFlex } from '../../utils/styles';

const { Text } = Typography;

const layout = {
  labelCol: { span: 8 },
  wrapperCol: { span: 16 },
};
const tailLayout = {
  wrapperCol: { offset: 8, span: 16 },
};

const FormContainer = styled.div`
  height: 100%;
  width: 100%;
  ${centerAllFlex}
`;

const SubmitButton = styled.div`
  margin-bottom: 10px;
`;

interface ModalProps {
  env: string;
  visible: boolean;
  handleOk: (accountData: Account) => void;
  handleCancel: () => void;
  hideModal: () => void;
}

const AddAccountModal = (props: ModalProps): JSX.Element => {
  const [isPrepaid, setIsPrepaid] = useState(true);
  const [validForm, setValidForm] = useState(true);
  const [brand, setBrand] = useState('Brand');
  const brandList = ['Smart Postpaid', 'Smart Prepaid', 'Sun', 'TNT'];

  const handleOk = (e: { username: string; password: string }) => {
    setValidForm(true);
    if (brand !== 'Brand') {
      const account: Account = {
        username: e.username,
        password: e.password,
        details: {
          brand: brand,
          isPrepaid: !isPrepaid,
        },
      };
      props.handleOk(account as Account);
    } else {
      setValidForm(false);
    }
  };

  const prepaidToggle = (checked: boolean) => {
    setIsPrepaid(checked);
  };

  const handleMenuClick = (e: MenuInfo) => {
    setValidForm(true);
    setBrand(brandList[+e.key]);
  };

  const BrandMenu = (
    <Menu onClick={(e) => handleMenuClick(e)}>
      {brandList.map((brandName: string, key: number) => (
        <Menu.Item key={key} icon={<FaSimCard color="rgba(251, 178, 23, 1)" />}>
          {brandName}
        </Menu.Item>
      ))}
    </Menu>
  );

  return (
    <Modal
      onCancel={() => props.hideModal()}
      title={`Add new account for ${props.env.toUpperCase()} environment`}
      visible={props.visible}
      footer={null}
    >
      <FormContainer>
        <Form {...layout} name="basic" onFinish={handleOk}>
          <Form.Item label="Username" name="username" rules={[{ required: true, message: 'Please input a username!' }]}>
            <Input />
          </Form.Item>
          <Form.Item label="Password" name="password" rules={[{ required: true, message: 'Please input a password!' }]}>
            <Input.Password />
          </Form.Item>
          <Form.Item label="Brand">
            <Dropdown overlay={BrandMenu}>
              <Button style={{ width: 160 }}>
                <FaSimCard color="rgba(251, 178, 23, 1)" style={{ marginRight: 5 }} /> {brand} <DownOutlined />
              </Button>
            </Dropdown>
            <div>{!validForm && <Text type="danger">Brand is required!</Text>}</div>
          </Form.Item>
          <Form.Item label="Sim Type" name="simType">
            <Switch
              checked={isPrepaid}
              defaultChecked
              checkedChildren="Postpaid"
              unCheckedChildren="Prepaid"
              onChange={prepaidToggle}
            />
          </Form.Item>
          <Form.Item {...tailLayout}>
            <SubmitButton>
              <Button type="primary" htmlType="submit">
                Submit
              </Button>
            </SubmitButton>
            <Button type="link" onClick={props.handleCancel}>
              Cancel
            </Button>
          </Form.Item>
        </Form>
      </FormContainer>
    </Modal>
  );
};

export default AddAccountModal;
