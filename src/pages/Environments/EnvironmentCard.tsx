import { Tooltip } from 'antd';
import React from 'react';
import { BiCloudDownload } from 'react-icons/bi';
import styled from 'styled-components';
// import { downloadFile } from '../../utils/download';

const OutsideDiv = styled.div`
  display: inline-block;
  padding: 20px;
`;

const EnvDiv = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  flex-direction: column;
  border: 1px solid lightgray;
  border-radius: 5px;
  width: 200px;
  height: 100px;
  padding: 0 20px;
  font-size: 1.2em;
  margin: 5px;
  background: rgba(255, 255, 255, 0.6);
`;

const IconWrapper = styled(BiCloudDownload)`
  cursor: pointer;
  color: rgba(231, 45, 141, 1);
  transition-duration: 0.3s;

  &:hover {
    transition-duration: 0.3s;
    color: rgba(251, 178, 23, 1);
  }
`;

const Title = styled.p`
  background: linear-gradient(90deg, rgba(2, 0, 36, 1) 0%, rgba(251, 178, 23, 1) 0%, rgba(231, 45, 141, 1) 100%);
  -webkit-background-clip: text;
  background-clip: text;
  -webkit-text-fill-color: transparent;
  font-weight: bold;
`;

interface EnvironmentDetails {
  downloadURL?: string;
  title?: string;
}

const EnvironmentCard = (props: EnvironmentDetails): JSX.Element => (
  <OutsideDiv>
    <EnvDiv>
      <Tooltip title={'Open ' + props.title}>
        <a target="_blank" href={props.downloadURL} rel="noreferrer">
          <IconWrapper size={30} />
        </a>
      </Tooltip>
      <Title>{props.title}</Title>
    </EnvDiv>
  </OutsideDiv>
);

export default EnvironmentCard;
