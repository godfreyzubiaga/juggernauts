import { Empty, Input } from 'antd';
import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import styled from 'styled-components';
import { v4 as uuidv4 } from 'uuid';

import { transparentLogo } from '../../assets/images';
import LoadingOverlay from '../../components/LoadingOverlay';
import { retrieveEnvironments, searchEnvironment } from '../../redux/actions/environmentActions';
import { AppState } from '../../redux/types';
import EnvironmentCard from './EnvironmentCard';

const Container = styled.div`
  margin: 0 auto;
  padding: 50px;
  height: 100vh;
`;

const EnvironmentDivs = styled.div`
  display: block;
  border-radius: 20px;
  height: 100%;
  margin: 0 auto;
  text-align: center;
  overflow-y: auto;
  width: 100%;
`;

const NoDataContainer = styled.span`
  display: flex;
  flex-direction: column;
  height: 200px;
  justify-content: space-around;
`;

const InnerContainer = styled.div`
  padding: 20px;
  border-radius: 20px;
  height: 100%;
  margin: 0 auto;
  background: url(${transparentLogo}) no-repeat;
  background-size: 20%;
  background-position: center center;
  background-color: #fafafa;
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
`;

const EnvironmentsPage = (): JSX.Element => {
  const dispatch = useDispatch();
  const environmentState = useSelector((state: AppState) => state.environments);

  useEffect(() => {
    dispatch(retrieveEnvironments());
  }, []);

  const onSearch = (searchKey: string) => {
    dispatch(searchEnvironment(searchKey));
  };

  return (
    <Container>
      <InnerContainer>
        <Input.Search
          placeholder="Search Environment"
          allowClear
          onSearch={onSearch}
          style={{ width: 200 }}
          loading={environmentState.status.loading}
        />
        <EnvironmentDivs>
          {environmentState.status.loading && <LoadingOverlay />}
          {environmentState &&
            !environmentState.status.loading &&
            environmentState.environments.length > 0 &&
            environmentState.environments.map((environment) => (
              <EnvironmentCard key={uuidv4()} title={environment.title} downloadURL={environment.url} />
            ))}
          {!environmentState.status.loading &&
            environmentState?.environments &&
            environmentState.environments.length === 0 && (
              <NoDataContainer>
                <Empty description="No Data" />
              </NoDataContainer>
            )}
        </EnvironmentDivs>
      </InnerContainer>
    </Container>
  );
};

export default EnvironmentsPage;
