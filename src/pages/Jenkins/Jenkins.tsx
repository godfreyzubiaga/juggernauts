import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useHistory } from 'react-router';
import styled from 'styled-components';
import { v4 as uuidv4 } from 'uuid';
import { transparentLogo } from '../../assets/images';

import { retrieveJenkins } from '../../redux/actions/jenkinsActions';
import { AppState } from '../../redux/types';
import { centerAllFlex } from '../../utils/styles';
import JenkinsLinks from './JenkinsLinks';

const Container = styled.div`
  margin: 0 auto;
  padding: 50px;
  height: 100vh;
`;

const InnerContainer = styled.div`
  height: 100%;
  width: 100%;
  border: 1px solid #f9f9f9;
  border-radius: 20px;
  background: url(${transparentLogo}) no-repeat;
  background-size: 20%;
  background-position: center center;
  background-color: #fafafa;
  padding: 20px;
  ${centerAllFlex}
`;

const LinksContainer = styled.div`
  width: 50%;
  ${centerAllFlex}
  justify-content: space-around;
`;

const JenkinsPage = (): JSX.Element => {
  const jenkins = useSelector((state: AppState) => state.jenkins);
  const history = useHistory();
  const dispatch = useDispatch();

  useEffect(() => {
    const depot = location.pathname.split('/')[2];
    dispatch(retrieveJenkins(depot));
  }, [history.location.pathname]);

  return (
    <Container>
      <InnerContainer>
        <LinksContainer>
          {jenkins.links &&
            jenkins.links.length > 0 &&
            jenkins.links.map((link) => <JenkinsLinks key={uuidv4()} {...link} />)}
        </LinksContainer>
      </InnerContainer>
    </Container>
  );
};

export default JenkinsPage;
