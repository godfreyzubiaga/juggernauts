import React from 'react';
import styled from 'styled-components';
import { Link } from '../../redux/reducers/jenkinsReducer';

const LinkDiv = styled.div`
  padding: 10px 5px;
  text-align: center;
  min-width: 300px;
  border: 1px solid lightgray;
  border-radius: 10px;
  color: black;
  font-size: 1.1em;
  font-weight: bold;
  transition-duration: 0.3s;
  background: linear-gradient(90deg, rgba(2, 0, 36, 1) 0%, rgba(251, 178, 23, 1) 0%, rgba(231, 45, 141, 1) 100%);
  -webkit-background-clip: text;
  background-clip: text;
  -webkit-text-fill-color: transparent;
  background-color: rgba(255, 255, 255, 1);

  &:hover {
    color: white;
    transition-duration: 0.3s;
    opacity: 1;
    background: linear-gradient(90deg, rgba(2, 0, 36, 1) 0%, rgba(251, 178, 23, 1) 0%, rgba(231, 45, 141, 1) 100%);
    -webkit-text-fill-color: white;
  }
`;

const JenkinsLinks = ({ link, title }: Link): JSX.Element => (
  <a rel="noreferrer" target="_blank" href={link}>
    <LinkDiv>{title}</LinkDiv>
  </a>
);

export default JenkinsLinks;
