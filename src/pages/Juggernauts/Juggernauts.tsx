import React from 'react';
import styled from 'styled-components';

import { logo } from '../../assets/images';
import DigitalDateClock from '../../components/DigitalDateClock/DigitalDateClock';
import { centerAllFlex } from '../../utils/styles';
import TeamCard from './TeamCard';

const MainContainer = styled.div`
  height: 100%;
  width: 100%;
  flex-direction: column;
  ${centerAllFlex}
  justify-content: space-around;
`;

const LogoContainer = styled.div`
  height: 200px;
  width: 400px;
  margin: 20px auto 20px auto;
  display: flex;
  justify-content: center;
  align-items: center;
  flex-direction: column;
`;

const Logo = styled.img`
  background-size: contain;
  background-position: center center;
  height: 100%;
`;

const ManilaText = styled.span`
  font-size: 1.5em;
  /* font-weight: bold; */
  letter-spacing: 3px;
`;

const OrgChartContainer = styled.div`
  min-height: 400px;
  min-width: 500px;
  display: flex;
  justify-content: space-evenly;
  align-items: center;
  flex-direction: column;
`;

const TLContainer = styled.div``;

const MembersContainer = styled.div`
  width: 500px;
  display: flex;
  justify-content: space-evenly;
  flex-direction: row;
`;

const uiDevelopers = ['April Hernandez', 'Godfrey Zubiaga', 'Hank Gonzales', 'Jazmin Jimenez', 'Zairous Latorena'];
const esbDevelopers = ['Anjo Marie Chua', 'Arnel Tristan Cruz', 'Lamberto Bruno'];

const Homepage = (): JSX.Element => {
  return (
    <MainContainer>
      <LogoContainer>
        <Logo src={logo} alt="Juggernaut" />
        <ManilaText>(Manila R&D)</ManilaText>
      </LogoContainer>
      <OrgChartContainer>
        <TLContainer>
          <TeamCard cartTitle="Team Lead" members={['Jose Roy Javelosa']} />
        </TLContainer>
        <MembersContainer>
          <TeamCard cartTitle="UI Developers" members={uiDevelopers} />
          <TeamCard cartTitle="ESB Developers" members={esbDevelopers} />
        </MembersContainer>
      </OrgChartContainer>
      <DigitalDateClock />
    </MainContainer>
  );
};

export default Homepage;
