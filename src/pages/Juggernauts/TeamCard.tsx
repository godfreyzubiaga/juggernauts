import React from 'react';
import styled from 'styled-components';
import { v4 as uuidv4 } from 'uuid';

import { centerAllFlex } from '../../utils/styles';

const Card = styled.div`
  width: 200px;
  border: 1px solid lightgray;
  text-align: center;
  border-radius: 10px;
`;

const TitleCard = styled.div`
  height: 30px;
  border-bottom: 1px solid lightgray;
  box-sizing: border-box;
  font-weight: bold;
  font-size: 1em;
  ${centerAllFlex}
  background: linear-gradient(90deg, rgba(2, 0, 36, 1) 0%, rgba(251, 178, 23, 1) 0%, rgba(231, 45, 141, 1) 100%);
  -webkit-background-clip: text;
  background-clip: text;
  -webkit-text-fill-color: transparent;
`;

const Members = styled.div`
  min-height: 50px;
  box-sizing: border-box;
  ${centerAllFlex}
  justify-content: space-around;
  flex-direction: column;
  padding: 20px 0;
`;

interface TeamData {
  cartTitle: string;
  members: string[];
}

const TeamCard = (data: TeamData): JSX.Element => (
  <Card>
    <TitleCard>{data.cartTitle}</TitleCard>
    <Members>
      {data.members.length > 0 && data.members.map((member: string) => <div key={uuidv4()}>{member}</div>)}
    </Members>
  </Card>
);

export default TeamCard;
