import { notification, Tooltip } from 'antd';
import copy from 'copy-to-clipboard';
import React from 'react';
import { BiCopyAlt } from 'react-icons/bi';
import styled from 'styled-components';
import { v4 as uuidv4 } from 'uuid';
import { transparentLogo } from '../../assets/images';

import links from '../../mockData/links.json';
import OptimaLink from './OptimaLink';

const Container = styled.div`
  padding: 50px;
  height: 100vh;
`;

const LinksContainer = styled.div`
  height: 100%;
  width: 100%;
  border: 1px solid #f9f9f9;
  border-radius: 20px;
  background: url(${transparentLogo}) no-repeat;
  background-size: 20%;
  background-position: center center;
  background-color: #fafafa;
  padding: 20px;
`;

const LinkDivider = styled.div`
  margin-bottom: 5px;
  display: flex;
`;

const CopyContainer = styled.div`
  display: flex;
  align-items: center;
  justify-items: start;
  cursor: pointer;
`;

const LinksPage = (): JSX.Element => {
  const copyToClipboard = (text: string, title: string) => {
    copy(text);
    notification.success({
      message: title,
      description: 'Succesfully added to your clipboard!',
      placement: 'bottomRight',
    });
  };

  return (
    <Container>
      <LinksContainer>
        {links.map((link) => (
          <LinkDivider key={uuidv4()}>
            <Tooltip placement="left" title="Copy">
              <CopyContainer onClick={() => copyToClipboard(link.url, link.title)}>
                <BiCopyAlt color="rgba(251, 178, 23, 1)" size="1.3em" />
              </CopyContainer>
            </Tooltip>
            <OptimaLink {...link} />
          </LinkDivider>
        ))}
      </LinksContainer>
    </Container>
  );
};

export default LinksPage;
