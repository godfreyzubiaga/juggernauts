import React from 'react';
import styled from 'styled-components';

const OptimaLinkText = styled.a`
  background: linear-gradient(90deg, rgba(2, 0, 36, 1) 0%, rgba(251, 178, 23, 1) 0%, rgba(231, 45, 141, 1) 100%);
  -webkit-background-clip: text;
  background-clip: text;
  -webkit-text-fill-color: transparent;
  transition-duration: 0.3s;
  font-size: 1.1em;
  padding: 5px 10px;
  border-radius: 5px;
  margin-left: 10px;
  font-weight: bold;

  &:hover {
    transition-duration: 0.3s;
    background: linear-gradient(270deg, rgba(2, 0, 36, 1) 0%, rgba(251, 178, 23, 1) 0%, rgba(231, 45, 141, 1) 100%);
    color: white;
    -webkit-text-fill-color: white;
    /* -webkit-background-clip: text;
    background-clip: text; */
    /* -webkit-text-fill-color: transparent; */
  }
`;

export interface OptimaLinkProps {
  url: string;
  title: string;
}

const OptimaLink = (link: OptimaLinkProps): JSX.Element => (
  <OptimaLinkText href={link.url} target="_blank">
    {link.title}
  </OptimaLinkText>
);

export default OptimaLink;
