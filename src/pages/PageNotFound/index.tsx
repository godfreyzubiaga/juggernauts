import React from 'react';
import { NavLink } from 'react-router-dom';
import styled from 'styled-components';

import { logoIcon } from '../../assets/images';
import Button from '../../components/Button';
import sizes from '../../utils/screens';

const StyledDiv = styled.div`
  min-height: calc(100vh - 80px);
  display: flex;
  justify-content: center;
  align-items: center;
  flex-direction: column;

  @media (max-width: ${sizes.laptop}px) {
    margin-top: 0;
  }
`;

const Container = styled.div`
  height: 500px;
  display: flex;
  justify-content: space-evenly;
  align-items: center;
  flex-direction: column;

  @media (max-width: ${sizes.laptop}px) {
    height: 100%;
  }
`;

const Title = styled.p`
  margin: 0;
  font-size: 2em;
  font-weight: bold;
`;

const Subtitle = styled.p`
  margin: 0;
  font-size: 1.5em;
`;

const LogoIcon = styled.div`
  background: url(${logoIcon}) no-repeat;
  background-size: contain;
  background-position: center center;
  height: 200px;
  width: 200px;
  /* border: 1px solid black; */
`;

const PageNotFound = (): JSX.Element => (
  <StyledDiv>
    <Container>
      <LogoIcon />
      <Subtitle>Page might be still under construction.</Subtitle>
      <Title>404 Page not Found!</Title>
      <NavLink to="/">
        <Button type="button" danger name="&larr; Go Back to Home" onClick={() => ''} />
      </NavLink>
    </Container>
  </StyledDiv>
);

export default PageNotFound;
