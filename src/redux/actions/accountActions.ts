import { notification } from 'antd';
import { Dispatch } from 'redux';
import { fbDatabase } from '../../index';
import { Account } from '../reducers/accountReducer';

export const BEGIN_RETRIEVE_ACCOUNTS = '@@/BEGIN_RETRIEVE_ACCOUNTS';
export const SUCCESS_RETRIEVE_ACCOUNTS = '@@/SUCCESS_RETRIEVE_ACCOUNTS';
export const FAILED_RETRIEVE_ACCOUNTS = '@@/FAILED_RETRIEVE_ACCOUNTS';
export const BEGIN_ADD_ACCOUNTS = '@@/BEGIN_ADD_ACCOUNTS';
export const SUCCESS_ADD_ACCOUNTS = '@@/SUCCESS_ADD_ACCOUNTS';
export const FAILED_ADD_ACCOUNTS = '@@/FAILED_ADD_ACCOUNTS';

export type recognizedENV = 'pt130' | 'pt03' | 'env4';

export const retrieveAccounts = (env: recognizedENV, brand?: string) => async (dispatch: Dispatch): Promise<void> => {
  try {
    if (env) {
      dispatch({ type: BEGIN_RETRIEVE_ACCOUNTS });
      const snapshot = await fbDatabase.ref('/accounts/' + env).get();
      const accounts: Account[] = snapshot.val() || [];
      if (accounts) {
        if (!brand || brand === 'All') {
          dispatch({ type: SUCCESS_RETRIEVE_ACCOUNTS, payload: accounts });
        } else {
          const filteredAccounts = accounts.filter((account: Account) => account.details.brand === brand);
          dispatch({ type: SUCCESS_RETRIEVE_ACCOUNTS, payload: filteredAccounts });
        }
      } else {
        throw new Error('Something went wrong while retrieving accounts.');
      }
    } else {
      throw new Error('Unrecognized Environment');
    }
  } catch (error) {
    dispatch({ type: FAILED_RETRIEVE_ACCOUNTS });
  }
};

export const addAccount = (env: recognizedENV, accountData: Account) => async (dispatch: Dispatch): Promise<void> => {
  try {
    if (env && accountData) {
      dispatch({ type: BEGIN_ADD_ACCOUNTS });
      const snapshot = fbDatabase.ref('/accounts/' + env);
      const snapshotRef = await snapshot.get();
      const accounts: Account[] = snapshotRef.val() || [];
      const newAccounts = [...accounts, accountData];
      const success = await snapshot.set(newAccounts, (a) => a);
      if (accounts && !success) {
        notification.success({
          duration: 3,
          message: 'Thank you!',
          description: 'Succesfully added a new account to ' + env.toUpperCase() + ' environment.',
          placement: 'bottomRight',
        });
        dispatch({ type: SUCCESS_ADD_ACCOUNTS, payload: newAccounts });
      } else {
        throw new Error('Something went wrong while retrieving accounts.');
      }
    } else {
      throw new Error('Unrecognized Environment');
    }
  } catch (error) {
    dispatch({ type: FAILED_ADD_ACCOUNTS });
  }
};
