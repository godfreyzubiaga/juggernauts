import { Dispatch } from 'redux';
import { fbDatabase } from '../..';
import { EnvironmentData } from '../reducers/environmentReducer';

export const BEGIN_RETRIEVE_ENVIRONMENTS = '@@/BEGIN_RETRIEVE_ENVIRONMENTS';
export const SUCCESS_RETRIEVE_ENVIRONMENTS = '@@/SUCCESS_RETRIEVE_ENVIRONMENTS';
export const FAILED_RETRIEVE_ENVIRONMENTS = '@@/FAILED_RETRIEVE_ENVIRONMENTS';
export const BEGIN_SEARCH_ENVIRONMENTS = '@@/BEGIN_SEARCH_ENVIRONMENTS';
export const SUCCESS_SEARCH_ENVIRONMENTS = '@@/SUCCESS_SEARCH_ENVIRONMENTS';
export const FAILED_SEARCH_ENVIRONMENTS = '@@/FAILED_SEARCH_ENVIRONMENTS';

const getEnvLinksFromFB = async () => {
  const snapshot = await fbDatabase.ref('/environments').get();
  const environments: EnvironmentData[] = snapshot.val();
  return environments;
};

export const retrieveEnvironments = () => async (dispatch: Dispatch): Promise<void> => {
  try {
    dispatch({ type: BEGIN_RETRIEVE_ENVIRONMENTS });
    const environments = await getEnvLinksFromFB();
    if (environments && environments.length > 0) {
      dispatch({ type: SUCCESS_RETRIEVE_ENVIRONMENTS, payload: environments });
    } else {
      throw new Error('Something went wrong while retrieving environments.');
    }
  } catch (error) {
    dispatch({ type: FAILED_RETRIEVE_ENVIRONMENTS });
  }
};

export const searchEnvironment = (key: string) => async (dispatch: Dispatch): Promise<void> => {
  try {
    dispatch({ type: BEGIN_SEARCH_ENVIRONMENTS });
    const environments = await getEnvLinksFromFB();
    if (environments) {
      let result: EnvironmentData[] = [];
      if (environments.length > 0) {
        result = environments.filter((env) => env.title.toLocaleLowerCase().includes(key));
      }
      dispatch({ type: SUCCESS_SEARCH_ENVIRONMENTS, payload: result });
    } else {
      throw new Error('Something went wrong while retrieving environments.');
    }
  } catch (error) {
    dispatch({ type: FAILED_SEARCH_ENVIRONMENTS });
  }
};
