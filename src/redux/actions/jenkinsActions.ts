import { Dispatch } from 'redux';
import jenkinsBuild from '../../mockData/jenkinsBuild.json';

export const BEGIN_RETRIEVE_JENKINS_BUILD = '@@/BEGIN_RETRIEVE_JENKINS_BUILD';
export const SUCCESS_RETRIEVE_JENKINS_BUILD = '@@/SUCCESS_RETRIEVE_JENKINS_BUILD';
export const FAILED_RETRIEVE_JENKINS_BUILD = '@@/FAILED_RETRIEVE_JENKINS_BUILD';

export const retrieveJenkins = (depot: string) => (dispatch: Dispatch): void => {
  try {
    if (depot === 'core-csr' || depot === 'core-sc' || depot === 'pldt-csr' || depot === 'pldt-sc') {
      dispatch({ type: BEGIN_RETRIEVE_JENKINS_BUILD });
      const jenkinsLinks = jenkinsBuild[depot];
      if (jenkinsLinks) {
        dispatch({ type: SUCCESS_RETRIEVE_JENKINS_BUILD, payload: jenkinsLinks });
      } else {
        throw new Error('Something went wrong while retrieving Jenkins Links.');
      }
    } else {
      throw new Error('Unrecognized Depot');
    }
  } catch (error) {
    dispatch({ type: FAILED_RETRIEVE_JENKINS_BUILD });
  }
};
