import {
  BEGIN_RETRIEVE_ACCOUNTS,
  FAILED_RETRIEVE_ACCOUNTS,
  SUCCESS_RETRIEVE_ACCOUNTS,
  BEGIN_ADD_ACCOUNTS,
  SUCCESS_ADD_ACCOUNTS,
  FAILED_ADD_ACCOUNTS,
} from '../actions/accountActions';
import { CallStatus, ReduxAction } from '../types';

const initialState: AccountState = {
  accounts: undefined,
  status: {
    loading: false,
  },
};

export interface Account {
  username: string;
  password: string;
  details: AccountDetails;
}

export interface AccountDetails {
  brand: string;
  isPrepaid: boolean;
}

export interface AccountState {
  accounts: Account[] | undefined;
  status: CallStatus;
}

const accountReducer = (state = initialState, action: ReduxAction): AccountState => {
  let newStatus: CallStatus;
  switch (action.type) {
    case BEGIN_RETRIEVE_ACCOUNTS:
    case BEGIN_ADD_ACCOUNTS:
      newStatus = { ...state.status, loading: true };
      return { ...state, status: newStatus, accounts: [] };
    case SUCCESS_RETRIEVE_ACCOUNTS:
    case SUCCESS_ADD_ACCOUNTS:
      newStatus = { ...state.status, loading: false, success: true };
      return { ...state, status: newStatus, accounts: [...(action.payload as Account[])] };
    case FAILED_RETRIEVE_ACCOUNTS:
    case FAILED_ADD_ACCOUNTS:
      newStatus = { ...state.status, loading: false, success: false };
      return { ...state, status: newStatus, accounts: [] };
    default:
      return { ...state };
  }
};

export default accountReducer;
