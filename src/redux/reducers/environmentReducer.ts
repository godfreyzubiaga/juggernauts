import {
  BEGIN_SEARCH_ENVIRONMENTS,
  SUCCESS_SEARCH_ENVIRONMENTS,
  FAILED_SEARCH_ENVIRONMENTS,
  BEGIN_RETRIEVE_ENVIRONMENTS,
  FAILED_RETRIEVE_ENVIRONMENTS,
  SUCCESS_RETRIEVE_ENVIRONMENTS,
} from '../actions/environmentActions';
import { CallStatus, ReduxAction } from '../types';

const initialState: EnvironmentState = {
  environments: [],
  status: {
    loading: false,
  },
};

export interface EnvironmentData {
  url: string;
  title: string;
}

export interface EnvironmentState {
  environments: EnvironmentData[];
  status: CallStatus;
}

const environmentReducer = (state = initialState, action: ReduxAction): EnvironmentState => {
  let newStatus: CallStatus;
  switch (action.type) {
    case BEGIN_RETRIEVE_ENVIRONMENTS:
    case BEGIN_SEARCH_ENVIRONMENTS:
      newStatus = { ...state.status, loading: true };
      return { ...state, status: newStatus, environments: [] };
    case SUCCESS_RETRIEVE_ENVIRONMENTS:
    case SUCCESS_SEARCH_ENVIRONMENTS:
      newStatus = { ...state.status, loading: false, success: true };
      return { ...state, status: newStatus, environments: [...(action.payload as EnvironmentData[])] };
    case FAILED_RETRIEVE_ENVIRONMENTS:
    case FAILED_SEARCH_ENVIRONMENTS:
      newStatus = { ...state.status, loading: false, success: false };
      return { ...state, status: newStatus, environments: [] };
    default:
      return { ...state };
  }
};

export default environmentReducer;
