import {
  BEGIN_RETRIEVE_JENKINS_BUILD,
  FAILED_RETRIEVE_JENKINS_BUILD,
  SUCCESS_RETRIEVE_JENKINS_BUILD,
} from '../actions/jenkinsActions';
import { CallStatus, ReduxAction } from '../types';

const initialState: JenkinsState = {
  links: [],
  status: {
    loading: false,
  },
};

export interface Link {
  link: string;
  title: string;
}

export interface JenkinsState {
  links: Link[];
  status?: CallStatus;
}

const jenkinsReducer = (state = initialState, action: ReduxAction): JenkinsState => {
  let newStatus: CallStatus;
  switch (action.type) {
    case BEGIN_RETRIEVE_JENKINS_BUILD:
      newStatus = { ...state.status, loading: true };
      return { ...state, status: newStatus, links: [] };
    case SUCCESS_RETRIEVE_JENKINS_BUILD:
      newStatus = { ...state.status, loading: false, success: true };
      return { ...state, status: newStatus, links: [...(action.payload as Link[])] };
    case FAILED_RETRIEVE_JENKINS_BUILD:
      newStatus = { ...state.status, loading: false, success: false };
      return { ...state, status: newStatus };
    default:
      return { ...state };
  }
};

export default jenkinsReducer;
