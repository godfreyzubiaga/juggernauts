import paths from '../../mockData/paths.json';
import { ReduxAction } from '../types';

const customPaths: PathState = paths;

export interface Path {
  key: string;
  title: string;
}

export interface PathState {
  accounts: Path[];
  jenkins: Path[];
  juggernauts: Path[];
}

const pathReducer = (state = customPaths, action: ReduxAction): PathState => {
  switch (action.payload) {
    default:
      return state;
  }
};

export default pathReducer;
