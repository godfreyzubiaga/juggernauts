import { TypedUseSelectorHook, useSelector } from 'react-redux';
import { combineReducers } from 'redux';

import { AppState } from '../types';
import accountReducer from './accountReducer';
import environmentReducer from './environmentReducer';
import jenkinsReducer from './jenkinsReducer';
import pathReducer from './pathReducers';

export const useTypedSelector: TypedUseSelectorHook<AppState> = useSelector;

export const rootReducer = combineReducers({
  paths: pathReducer,
  account: accountReducer,
  jenkins: jenkinsReducer,
  environments: environmentReducer,
});

export default rootReducer;
