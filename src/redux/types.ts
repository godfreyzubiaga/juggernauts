import { PathState } from './reducers/pathReducers';
import { JenkinsState } from './reducers/jenkinsReducer';
import { AccountState } from './reducers/accountReducer';
import { EnvironmentState } from './reducers/environmentReducer';

export interface Sample {
  init: string;
}

export interface Path {
  key: string;
  title: string;
}

export interface ReduxAction {
  type: string;
  payload: unknown;
}

export interface CallStatus {
  loading: boolean;
  success?: boolean;
}

export interface AppState {
  paths: PathState;
  account: AccountState;
  jenkins: JenkinsState;
  environments: EnvironmentState;
}
