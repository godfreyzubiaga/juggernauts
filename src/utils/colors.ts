export const primaryColor = '#007cdc';

export const activePrimaryColor = '#036dbf';

export const secondaryColor = '#C044D8';

export const activeSecondaryColor = '#B13EC7';

export const dangerColor = '#DA5F5F';

export const activeDangerColor = '#C85353';

export const white = '#f8f8ff';

export const activeWhite = '#e4e4e9';

export const black = '#131313';

export const activeBlack = '#171717';
