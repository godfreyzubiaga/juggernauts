const sizes = {
  phone: 425,
  tablet: 768,
  laptop: 992,
  desktop: 1170,
};

export default sizes;
